var check_ ={
    finishAnimation:{
        "showImage":function(){
            var element = $('<div class="finalImg"></div>');
            element.css({"width":"100%","height":"100%","top":0,"left":0});
            if(task[currentTask].settings.onFinishAnimation.style)
                element.css(task[currentTask].settings.onFinishAnimation.style);
            for(var key in task[currentTask].settings.onFinishAnimation.positions)
                setSize(element, key, task[currentTask].settings.onFinishAnimation.positions[key]);
            getContainer().append(element);
        }
    },
    "startAnimation":function(){
        var finishdiv = $('<div class="finishDiv"></div>');
        finishdiv.css('opacity',0);
        getContainer().append(finishdiv);
        finishdiv.animate({"opacity":1});
        setTimeout(function(el){el.animate({"opacity":0});},2000, finishdiv);
        if(task[currentTask].settings && task[currentTask].settings.onFinishAnimation && task[currentTask].settings.onFinishAnimation.type && check_.finishAnimation[task[currentTask].settings.onFinishAnimation.type]){
            check_.finishAnimation[task[currentTask].settings.onFinishAnimation.type]();
        }
    }
};