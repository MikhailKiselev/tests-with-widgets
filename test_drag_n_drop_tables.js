var test_drag_n_drop_tables = {
	defaultplaces:{},
	create:function(input){
		//alert(JSON.stringify(input));
		var result = [];
		for(var i=0;i<input.length;i++){
			if(input[i].answers)
				if(input[i].answers.answers)
					for(var j=0;j<input[i].answers.answers.length;j++){
						var element = $('<div id="table'+i+'draggable'+j+'" class="draggable"></div>');
						var defaultPlace = $('<div id="table'+i+'defaultPlace'+j+'" class="defaultPlace"></div>');
						test_drag_n_drop_tables.defaultplaces[defaultPlace[0].id] = element[0].id;
						if(input[i].answers.pattern){
							if(input[i].answers.pattern.style)
								element.css(input[i].answers.pattern.style);
							if(input[i].answers.pattern.positions)
								for(var key in input[i].answers.pattern.positions){
									setSize(element, key, input[i].answers.pattern.positions[key]);
									setSize(defaultPlace, key, input[i].answers.pattern.positions[key]);
								}
							if(input[i].answers.pattern.onStopDragging)
								element.data('stopdragging', input[i].answers.pattern.onStopDragging);
							else
								element.data('stopdragging','leave');
						}
						if(input[i].answers.answers[j].style)
							element.css(input[i].answers.answers[i].style);
						if(input[i].answers.answers[j].positions)
							for(var key in input[i].answers.answers[i].positions){
								setSize(element,key,input[i].answers.answers[j].positions[key]);
								setSize(defaultPlace,key,input[i].answers.answers[j].positions[key]);
							}
						defaultPlace.droppable({
							out: test_drag_n_drop_tables.defaultPlaceHandleOutEvent,
							drop: test_drag_n_drop_tables.defaultPlaceHandleDropEvent
						});
						if(input[i].answers.answers[j].hint)
							element.data('hint',input[i].answers.answers[j].hint);
						if(input[i].answers.answers[j].onStopDragging)
							element.data('stopdragging', input[i].answers.answers[j].onStopDragging);
						if(input[i].answers.answers[j].value){
							var inner = $('<div>'+input[i].answers.answers[j].value+'</div>');
							inner.css('width', element.css('width'));
							inner.css('height',element.css('height'));
							if(input[i].answers.answers[j].textShow=="removeOnTarget")
								inner.addClass('answerText1');
							if(input[i].answers.pattern.innerStyle)
								inner.css(input[i].answers.pattern.innerStyle);
							if(input[i].answers.answers[j].innerStyle)
								inner.css(input[i].answers.answers[j].innerStyle);
							if(input[i].answers.pattern.innerPositions)
								for(var key in input[i].answers.pattern.innerPositions)
									setSize(inner, key,input[i].answers.pattern.innerPositions[key]);
							if(input[i].answers.answers[j].innerPositions)
								for(var key in input[i].answers.answers[j].innerPositions)
									setSize(inner, key,input[i].answers.answers[j].innerPositions[key]);
							
							inner.html(input[i].answers.answers[j].value);
							element.append(inner);
						}
						element.mousedown(function(){
							$(this).addClass('active');
						});
						if(input[i].answers.answers[j].renewable){
							element.mouseup(function(){
								$(this).removeClass('active');
								setTimeout(function(a){
									if(!a.hasClass('in-the-droppable')
										&& test_drag_n_drop_tables.defaultplaces['table'+test_drag_n_drop_tables.numberFromId(a[0].id,true)+'defaultPlace'+test_drag_n_drop_tables.numberFromId(a[0].id,false)]!=a[0].id)
										{
											a.remove();
										}
								},100,$(this));
							});
						} else {
							element.mouseup(function(){$(this).removeClass('active');test_drag_n_drop.checkInDroppable();});
						}
						
						result.push(element);
						result.push(defaultPlace);
						setTimeout(function(a){
							a.draggable({
								stop:test_drag_n_drop_tables.handleStopDraggingEvent
							});
						},100,element);
					}
			if(input[i].correctAnswers && input[i].correctAnswers.values && input[i].correctAnswers.values[0]){
				for(var j=0;j<input[i].correctAnswers.values[0].length;j++){
					
					for(var k=0;k<input[i].correctAnswers.values[0][j].length;k++){
						var element = $('<div id="table'+i+'droppable'+j+'_'+k+'" class="droppable"></div>');
						if(input[i].correctAnswers.pattern){
							if(input[i].correctAnswers.pattern.style)
								element.css(input[i].correctAnswers.pattern.style);
							if(input[i].correctAnswers.pattern.positions)
								for(var key in input[i].correctAnswers.pattern.positions)
									setSize(element,key,input[i].correctAnswers.pattern.positions[key]);
						}
						if(input[i].correctAnswers.styles && input[i].correctAnswers.styles[j] && input[i].correctAnswers.styles[j][k])
							element.css(input[i].correctAnswers.styles[j][k]);
						if(input[i].correctAnswers.positions && input[i].correctAnswers.positions[j] && input[i].correctAnswers.positions[j][k])
							for(var key in input[i].correctAnswers.positions[j][k])
								setSize(element,key,input[i].correctAnswers.positions[j][k][key]);
						element.droppable({
							drop: test_drag_n_drop_tables.handleDropEvent,
							out: test_drag_n_drop_tables.handleOutEvent
						});
						result.push(element);
						if(!data[currentTask]) data[currentTask]={};
						if(data[currentTask][element.attr('id')])
							setTimeout(function(el){
								$('#'+data[currentTask][el.attr('id')]).css('top',el.css('top'));
								$('#'+data[currentTask][el.attr('id')]).css('left',el.css('left'));
							},100,element);
					}
				}
				
				
			}
		//		alert('ca');
		}
		//alert(test_drag_n_drop_tables.numberFromId('table4defaultPlace78',true));
		
		return $(result);
	},
	numberFromId:function(id,table){
		var arr = id.split('defaultPlace');//table'+i+'defaultPlace
		if(arr.length==1)
			var arr = id.split('draggable');
		if(arr.length==1)
			var arr = id.split('droppable');
		//if(arr.length==1)
			
				
		if(table){
			var string = arr[0].replace('table','');
		} else {
			var string = arr[1].split('duplicate')[0];
		}
		return parseInt(string);
	},
	handleStopDraggingEvent:function(event,ui){
        var draggable = $(event.target);
        if(!draggable.hasClass('in-the-droppable')){
            if(draggable.data('stopdragging')=='defaultplace'){
                draggable.animate({"top":$('#defaultPlace'+test_drag_n_drop.numberFromId(event.target.id)).css('top'),"left":$('#defaultPlace'+test_drag_n_drop.numberFromId(event.target.id)).css('left')})
            }
        }
	},
	defaultPlaceHandleDropEvent:function(event,ui){
        if(!test_drag_n_drop_tables.defaultplaces[event.target.id]){
            var draggable = ui.draggable;
            draggable.css('left', $(event.target).css('left'));
            draggable.css('top', $(event.target).css('top'));
            test_drag_n_drop_tables.defaultplaces[event.target.id]=draggable.attr('id');
        }
	},
	defaultPlaceHandleOutEvent:function(event,ui){
        var draggable = ui.draggable;
		//alert(task[currentTask]['test_drag_n_drop_tables'][test_drag_n_drop_tables.numberFromId(event.target.id,true)].answers.answers[test_drag_n_drop_tables.numberFromId(draggable[0].id)].renewable);
//alert(JSON.stringify(test_drag_n_drop_tables.defaultplaces));
        if(test_drag_n_drop_tables.defaultplaces[event.target.id]==draggable.attr('id')){
            test_drag_n_drop_tables.defaultplaces[event.target.id] = false;
            //alert(task[currentTask]['test_drag_n_drop_tables'][test_drag_n_drop_tables.numberFromId(event.target.id,true)].answers.answers[test_drag_n_drop_tables.numberFromId(draggable[0].id)].renewable);
			if(task[currentTask]['test_drag_n_drop_tables'][test_drag_n_drop_tables.numberFromId(event.target.id,true)].answers.answers[test_drag_n_drop_tables.numberFromId(draggable[0].id)].renewable)
				test_drag_n_drop_tables.addDuplicate(draggable[0].id);//.addDraggableDuplicate(test_drag_n_drop_tables.numberFromId(event.target.id));
        }
	},
	handleDropEvent:function(event,ui){
        var draggable = ui.draggable;
        if((!data[currentTask] || !data[currentTask][event.target.id]) && test_drag_n_drop_tables.numberFromId(event.target.id,true)==test_drag_n_drop_tables.numberFromId(draggable[0].id)){
            draggable.css('left', $(event.target).css('left'));
            draggable.css('top', $(event.target).css('top'));
            draggable.removeClass('shadow');
	        draggable.addClass('in-the-droppable');
	        $(event.target).addClass('withDraggable');
	        $(event.target).removeClass('emptyWrong');
            if(!data[currentTask]) data[currentTask]={};
            data[currentTask][event.target.id]=draggable.attr('id');

            if(task[currentTask].check.autoCheck){
                lastElementId = draggable.attr('id');
                check();
            }
		} else {
			if(test_drag_n_drop_tables.numberFromId(event.target.id,true)==test_drag_n_drop_tables.numberFromId(draggable[0].id,true)){
				draggable.addClass('in-the-droppable');
				if(!data[currentTask]) data[currentTask]={};
				var secondDraggable = $('#'+data[currentTask][event.target.id]);
				secondDraggable.removeClass('in-the-droppable');
				if(secondDraggable.data('stopdragging')=='defaultplace')
					secondDraggable.animate({"top":$('#defaultPlace'+test_drag_n_drop.numberFromId(data[currentTask][event.target.id])).css('top'),"left":$('#defaultPlace'+test_drag_n_drop.numberFromId(data[currentTask][event.target.id])).css('left')});
				else
					secondDraggable.css({"left":draggable.css('left'),"top":draggable.css('top')});

				data[currentTask][event.target.id]=draggable.attr('id');
				draggable.css('left', $(event.target).css('left'));
				draggable.css('top', $(event.target).css('top'));

				if(task[currentTask].check.autoCheck){
					lastElementId =  draggable.attr('id');
					check();
				}
			}
		}
	},
	handleOutEvent:function(event,ui){
		var draggable = ui.draggable;
		if(data[currentTask] && data[currentTask][event.target.id]==draggable.attr('id')){
			data[currentTask][event.target.id]=false;
		    draggable.removeClass('in-the-droppable');
		    draggable.removeClass('draggable-right');
			draggable.removeClass('draggable-wrong');
		    $(event.target).removeClass('withDraggable');
		    $(event.target).removeClass('withDraggableWrong');
		}
	},
	addDuplicate:function(id){
		//alert('duplicate');
		var tableNum = test_drag_n_drop_tables.numberFromId(id,true);
		var draggableNum = test_drag_n_drop_tables.numberFromId(id,false);
		//tableNum, draggableNum
		var element = $('<div id="table'+tableNum+'draggable'+draggableNum+'duplicate'+new Date().getTime()/1+'" class="draggable"></div>');
		test_drag_n_drop_tables.defaultplaces["table"+tableNum+"defaultPlace"+draggableNum] = element[0].id;
		if(task[currentTask]['test_drag_n_drop_tables'][tableNum].answers.pattern){
			var pattern = task[currentTask]['test_drag_n_drop_tables'][tableNum].answers.pattern;
			if(pattern.style)
				element.css(pattern.style);
			if(pattern.positions)
				for(var key in pattern.positions)
					setSize(element, key, pattern.positions[key]);
			
		}
		var elData = task[currentTask]['test_drag_n_drop_tables'][tableNum].answers.answers[draggableNum];
		if(elData.style)
			element.css(elData.style);
		if(elData.positions)
			for(var key in elData.positions)
				setSize(element,key, elData.positions[key]);
		if(elData.value){
			var inner = $('<div>'+elData.value+'</div>');
			inner.css('width', element.css('width'));
			inner.css('height',element.css('height'));
			if(elData.textShow=="removeOnTarget")
				inner.addClass('answerText1');
			if(pattern.innerStyle)
				inner.css(pattern.innerStyle);
			if(elData.innerStyle)
				inner.css(elData.innerStyle);
			if(pattern.innerPositions)
				for(var key in pattern.innerPositions)
					setSize(inner, key,pattern.innerPositions[key]);
			if(elData.innerPositions)
				for(var key in elData.innerPositions)
					setSize(inner, key,elData.innerPositions);
			inner.html(elData.value);
			element.append(inner);
		}
		element.mousedown(function(){
			$(this).addClass('active');
		});
		element.mouseup(function(){
			$(this).removeClass('active');
			setTimeout(function(a){
				if(!a.hasClass('in-the-droppable')
					&& test_drag_n_drop_tables.defaultplaces['table'+test_drag_n_drop_tables.numberFromId(a[0].id,true)+'defaultPlace'+test_drag_n_drop_tables.numberFromId(a[0].id,false)]!=a[0].id)
					{
						a.remove();
					}
			},100,$(this));
		});
		getContainer().append(element);
		element.draggable();
	},
	check:function(){//alert(JSON.stringify(data[currentTask]));
		for(var i=0;i<task[currentTask]['test_drag_n_drop_tables'].length;i++){
			var values = task[currentTask]['test_drag_n_drop_tables'][i].correctAnswers.values;
			
			for(var j=0;j<values.length;j++){
				var matrix = values[j];
				var matrixResult = true;
				for(var k=0;k<matrix.length;k++)
					for(var l=0; l<matrix[k].length;l++){
						if(!data[currentTask]) data[currentTask]={};
						//alert(task[currentTask]['test_drag_n_drop_tables'][test_drag_n_drop_tables.numberFromId(data[currentTask]['table'+i+'droppable'+k+'_'+l],true)].answers.answers[test_drag_n_drop_tables.numberFromId(data[currentTask]['table'+i+'droppable'+k+'_'+l])].value+'!='+matrix[k][l]);
						if(!data[currentTask]['table'+i+'droppable'+k+'_'+l] || task[currentTask]['test_drag_n_drop_tables'][test_drag_n_drop_tables.numberFromId(data[currentTask]['table'+i+'droppable'+k+'_'+l],true)].answers.answers[test_drag_n_drop_tables.numberFromId(data[currentTask]['table'+i+'droppable'+k+'_'+l])].value!=matrix[k][l]){
							matrixResult = false;
							//alert(task[currentTask]['test_drag_n_drop_tables'][test_drag_n_drop_tables.numberFromId(data[currentTask]['table'+i+'droppable'+k+'_'+l],true)].answers.answers[test_drag_n_drop_tables.numberFromId(data[currentTask]['table'+i+'droppable'+k+'_'+l])].value+" \n"+matrix[k][l]+"\n"+j);
						}
					}
				//alert(matrixResult);
				if(!matrixResult && j==task[currentTask]['test_drag_n_drop_tables'][i].correctAnswers.values.length-1){
					$(".in-the-droppable[id^=table"+i+"draggable]").addClass('draggable-wrong');
					return {"result":false,"score":0};
				}
					
				if(matrixResult){
					$("div[id*=table"+i+"draggable").addClass('draggable-right')
					$(".in-the-droppable[id^=table"+i+"draggable]").addClass('draggable-right');

					return {"result":true,"score":0};
				}
					
			}
		}
		return {"result":false,"score":0}
	}
	
}