var help ={
    enabled:false,

    alertHint:{
        init:function(){
            $(div).each(help.alertHint.showHint);
        },
        start:function(){},
        stop:function(){},
        showHint:function(){
            if($(this).data('hint') && help.enabled){
                alert($(this).data('hint'));
            }
        }
    },
    showHint:{
        init:function(){
            getContainer().children().each(function(){$(this).click(help.showHint.showHelp);});
            var overlay = $('<div class="overlay"></div>');
            var overlayHint = $('<div id="overlayHint"></div>');
            if(task[currentTask].help.params)
                if(task[currentTask].help.params.overlayHint){
                    if(task[currentTask].help.params.overlayHint.style)
                        overlayHint.css(task[currentTask].help.params.overlayHint.style);
                    if(task[currentTask].help.params.overlayHint.positions)
                        for(var key in task[currentTask].help.params.overlayHint.positions)
                            setSize(overlayHint,key,task[currentTask].help.params.overlayHint.positions[key]);
                }

            var overlayBg= $('<div class="overlayBg"></div>');
            var overlayBack = $('<div class="overlayBack">Вернуться к заданию</div>');
            setSize(overlayBack,'width',200);
            setSize(overlayBack,'height',35);
            overlayBack.click(
                function (){
                    $('#overlayHint').html('');
                    $('#content').removeClass('showhint');
                    help.enabled = false;
                    $('.overlay').hide();
                    $('#helpButton').show();
                });
            overlay.append(overlayHint).append(overlayBg).append(overlayBack);
            getContainer().append(overlay);
        },
        start:function(){
            $('.overlay').show();
            $('#helpButton').hide();
        },
        stop:function(){
            $('.overlay').hide();
			setTimeout(function(){help.enabled = false;},100);
        },
        showHelp:function(){
            if($(this).data('hint') && help.enabled){
                $('#overlayHint').html($(this).data('hint'));
            } 
        }
    },
	showShortHint:{
		init:function(){
			var hintDiv = $('<div id="hintDiv" class="hint-small"></div>');
			getContainer().children().each(function(){$(this).click(help.showShortHint.showHelp);});
			getContainer().append(hintDiv);
		},
		start:function(){
			$('#hintDiv').show();
			$('#hintDiv').css('top',$('#helpButton'+currentTask).css('top'));
			$('#hintDiv').css('height',$('#helpButton'+currentTask).css('height'));
		},
		stop:function(){
			$('#hintDiv').hide();
			help.enabled = false;
		},
		showHelp:function(){
			if($(this).data('hint') && help.enabled){
                $('#hintDiv').html($(this).data('hint'));
            } 
		}
	},
    showHTML:{
        init:function(){
            var overlay = $('<div class="overlay" style="z-index:6"></div>');
            var overlayContent = $('<div id="overlayContent" class="popup"></div>');
            //var overlayBg = $('<div class="overlayBg"></div>');
            if(task[currentTask].help.params)
				if(task[currentTask].help.params.html)
                    overlayContent.append(task[currentTask].help.params.html);
				if(task[currentTask].help.params.innerStyle)
					overlayContent.css(task[currentTask].help.params.innerStyle);
				if(task[currentTask].help.params.style)
					overlay.css(task[currentTask].help.params.style);
				if(task[currentTask].help.params.positions)
					for(var key in task[currentTask].help.params.positions){
						setSize(overlay, key, task[currentTask].help.params.positions[key])
					}
            overlay.append(overlayContent)//.append(overlayBg);
            getContainer().append(overlay);
            overlay.click(function(){$(this).hide();$('#helpButton').show();setTimeout(function(){help.enabled = false;},30);});
        },
        start:function(){
            $('.overlay').show();
            $('#helpButton').hide();
        },
        stop:function(){
			 $('.overlay').hide();
            $('#helpButton').show();
		}
    },
    showHelpIcon:{
        init:function(){
            var hintContainer = $('<div id="hintContainer" style="display:none"></div>');
            setSize(hintContainer,'margin-left',-30);
            setSize(hintContainer,'margin-top',-15);
            setSize(hintContainer,'border-radius',-5);
            getContainer().children().each(function(){
                if($(this).data('hint')){//help 
                    var helpIcon = $('<div id="H_'+this.id+'" class="question-mark" style="display:none;">?</div>');
                    helpIcon.css('top',(parseInt($(this).css('top'))-getSize(25))+'px');
                    setSize(helpIcon,'width',20);
                    setSize(helpIcon,'height',20);
                    setSize(helpIcon,'border-radius',10);
                    setSize(helpIcon,'font-size',15);
                    helpIcon.css('left',$(this).css('left'));
                    helpIcon.data('text',$(this).data('hint'));
                    getContainer().append(hintContainer).append(helpIcon);
                    helpIcon.click(function(e){
                        $('#hintContainer').html($(this).data('text'));
                        $('#hintContainer').css('top', (e.pageY-getContainer().offset().top)+'px');
                        $('#hintContainer').css('left',(e.pageX-getContainer().offset().left)+'px');
                        $('#hintContainer').show();
                    });
                    hintContainer.click(function(){
						$(this).hide();
						setTimeout(function(){help.enabled = false;},100);
					});
                }
            });
        },
        "start":function(){
            $("*").each(function(){
                if($(this).data('hint')){
                    $('#H_'+this.id).show();
                }
            });
        },
        "stop":function(){
            $("*").each(function(){
                if($(this).data('hint')){
                    $('#H_'+this.id).hide();
                }
            });
        }
    }
};