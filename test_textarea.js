var test_textarea = {

    create: function (input)
    {
        var result = [];
        for (var i = 0; i < input.length; i++)
        {
            var textarea = $('<input id="test_textarea'+i+'" type="text" class="textEditBox" value="" />');
            if(data[currentTask] && data[currentTask]['test_textarea'+i])
                textarea.val(data[currentTask]['test_textarea'+i]);
            if(input[i].hint)
                textarea.data('hint', input[i].hint);
            if (input[i].style)
            {
                textarea.css(input[i].style);
            }
            if (input[i].positions)
            {
                for (key in input[i].positions)
                {
                  setSize(textarea, key, input[i].positions[key]);
                }
            }
            if (input[i].maxlength)
            {
                textarea.attr("maxlength", input[i].maxlength);
            }
            textarea.bind("input", function ()
            {
				$(this).removeClass("textarea-right");
                $(this).removeClass("textarea-wrong");
                if (task[currentTask].check.autoCheck)
                    check();
                if (!data[currentTask])
                   data[currentTask] = {};
               data[currentTask][this.id] = this.value;
               if (this.maxLength && this.value.length == this.maxLength)
               {
                   $(this).next().focus();
               }
            });
            result.push(textarea);
        }
        return $(result);
    },
    check:function(){
        var result = true;
        if(task[currentTask]['test_textarea']){
            for (var i = 0; i < task[currentTask]['test_textarea'].length; i++){
                if (!test_textarea.checkTextEdit(i))
            {
                result = false;
            }
        }
    }
    return {"result":result,"score":0};;
    },
    checkTextEdit:function(n){
        dataObj=task[currentTask]['test_textarea'][n];
        var result = false;
        var answer = $('#test_textarea'+n).val();
        if (!dataObj.isCaseSensitive)
            answer = answer.toLowerCase();
        answer = answer.replace(/ /g, '');
        var tmpanswer = answer;
        if (dataObj.correctValues){
            for (var i = 0; i < dataObj.correctValues.length; i++){
                var correctAnswer = dataObj.correctValues[i];
                if (!dataObj.isCaseSensitive)
                    correctAnswer = correctAnswer.toLowerCase();
                correctAnswer = correctAnswer.replace(/ /g, '');
                if (correctAnswer == answer){
                    result = true;
                    break;
                }
            }
        }
        if (!result && dataObj.correctRange)
        {
            answer = parseInt(answer);
            result = answer >= dataObj.correctRange.start && answer <= dataObj.correctRange.stop;
        }
        var jObject = $('#test_textarea'+n);
        if (result){   //if(answer!='' && !isNaN(answer))
            if(showCheckAnimation() && (!lastElementId || lastElementId==jObject.attr('id'))) jObject.addClass("textarea-right");
        }
        else {   //if(answer!='' && !isNaN(answer))
            if((!lastElementId || lastElementId==jObject.attr('id'))) if(showCheckAnimation()) jObject.addClass("textarea-wrong");
        }

        /*setTimeout(function (a) {
            a.removeClass("textarea-right");
            a.removeClass("textarea-wrong");
        }, 1100, jObject);*/
    return result;
}
}