var images_texts = {
    create:function(input){
        var result = [];
        for (var i=0;i<input.length;i++){
            var imagetext = $('<div id="image'+i+'" class="image"></div>');
            if(task[currentTask]['images_texts'][i].style)
                imagetext.css(task[currentTask]['images_texts'][i].style);
            if(task[currentTask]['images_texts'][i].positions)
                for(key in task[currentTask]['images_texts'][i].positions){
                    setSize(imagetext,key,task[currentTask]['images_texts'][i].positions[key]);
                }
            if(task[currentTask]['images_texts'][i].value){
                var element = $('<div>'+task[currentTask]['images_texts'][i].value+'</div>');
                if(task[currentTask]['images_texts'][i].valuestyle)
                    element.css(task[currentTask]['images_texts'][i].valuestyle);
                if(task[currentTask]['images_texts'][i].valuepositions)
                    for(key in task[currentTask]['images_texts'][i].valuepositions){
                        setSize($(element),key,task[currentTask]['images_texts'][i].valuepositions[key]);
                    }
                imagetext.append(element);
            }
            result.push(imagetext);
        }
        return $(result);
    }

};