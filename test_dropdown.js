var test_dropdown = {
    create: function(input){
        if (!data[currentTask])
            data[currentTask] = {};

        var result = [];
        for (var i=0;i<input.length;i++){
            var wrap = $('<div class="select-wrap"></div>');
            var select = $('<select class="dropdown" id="dropdown'+i+'"></select>');

            if(input[i].hint)
                select.data('hint',input[i].hint);
            if (input[i].style){
                select.css(input[i].style);
            }
            if (input[i].positions) {
                for (key in input[i].positions){
                   setSize(wrap, key, input[i].positions[key]);
                   if(key=='height')
                       setSize(select, key, input[i].positions[key]);
                }
            }
            for(var j=0;j<input[i].items.length;j++){
                var selectedText='';
                if(data[currentTask] && data[currentTask]['test_dropdown'+i] && data[currentTask]['test_dropdown'+i]==j)
                    selectedText = ' selected';
                else {
                    data[currentTask]['test_dropdown'+i]=0;
                }
                select[0].onchange = function(){
					$(this).parent().removeClass("dropdown-wrong");
					$(this).parent().removeClass("dropdown-right");

                    data[currentTask]['test_'+this.id] = $(this).val();
                    if(task[currentTask].check.autoCheck){
                       lastElementId = $(this).attr('id');
                       check();
                    }
                }
                select.append($('<option value="'+j+'"'+selectedText+'>'+input[i].items[j]+'</option>'));
            }
            wrap.append(select);
            result.push(wrap);
        }
        return $(result);
    },
    check: function(){
        var result = true;
        if(task[currentTask]['test_dropdown']){
            for (var i = 0; i < task[currentTask]['test_dropdown'].length; i++){
                if(task[currentTask]['test_dropdown'][i].right!=parseInt(data[currentTask]['test_dropdown'+i])){
                    result = false;
                    if(showCheckAnimation() && (!lastElementId || lastElementId=='dropdown'+i)) $('#dropdown'+i).parent().addClass("dropdown-wrong");
                } else {
                    if(showCheckAnimation() && (!lastElementId || lastElementId=='dropdown'+i)) $('#dropdown'+i).parent().addClass("dropdown-right");
                }
                /*setTimeout(function (a) {
                    a.removeClass("correct");
                    a.removeClass("error");
                }, 1100, $('#dropdown'+i));*/
            }
        }
        return {"result":result,"score":0};;
    }
}