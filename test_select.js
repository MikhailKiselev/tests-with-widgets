var test_select = {
    create:function(input){
         var result = [];
        for(var i=0;i<input.length;i++){
            var element = $('<div id="'+task[currentTask]['test_select'][i].type+''+i+'" class="absoluteBlock"></div>');
            element.data('blockNum',i);
            element.data('blockType',task[currentTask]['test_select'][i].type);
            if(task[currentTask]['test_select'][i].hint)
                element.data('hint', task[currentTask]['test_select'][i].hint);
            if(task[currentTask]['test_select'][i].style)
                element.css(task[currentTask]['test_select'][i].style);
            if(task[currentTask]['test_select'][i].positions)
               for(var key in task[currentTask]['test_select'][i].positions)
                   setSize(element,key,task[currentTask]['test_select'][i].positions[key]);
            if(task[currentTask]['test_select'][i].title){
                element.append($('<div class="blockTitle">'+task[currentTask]['test_select'][i].title+'</div>'));
            }
            if(task[currentTask]['test_select'][i].type){
                var typeClass = task[currentTask]['test_select'][i].type=='manyOf' ? 'many':'one';
                
            } else
                var typeClass = 'one';
            if(task[currentTask]['test_select'][i].variants){
                var rowTop=0;
                var colLeft=0;
                var rowHeight=0;
                var variants = task[currentTask]['test_select'][i].variants;
               for(var j=1;j<variants.length;j++){
                   var variant = $('<div class="variant clickable '+typeClass+'"></div>');
                   if(data[currentTask] && data[currentTask][i]){
                       if(data[currentTask][i]==j || data[currentTask][i][j])
                           if(typeClass=='one')
                               variant.addClass("clickable-selected");
                           else
                               variant.addClass("selected");
                   }
                   variant.data('answer',j);
                   variant.click(test_select.variantClick);
                 if(variants[j].right){
                     var weight = variants[j].forRight ? variants[j].forRight : variants[0].forRight;
                } else {
                    var weight = variants[j].forWrong ? variants[j].forWrong*(-1) : variants[0].forWrong*(-1);
                }
                variant.data('weight', weight);
                if(variants[0].style)
                    variant.css(variants[0].style);
                if(variants[0].positions)
                    for(key in variants[0].positions)
                        setSize(variant, key, variants[0].positions[key]);
                if(variants[j].hint)
                    variant.data('hint',variants[j].hint);
                if(variants[j].style)
                    variant.css(variants[j].style);
                if(variants[j].positions)
                   for(key in variants[j].positions)
                   setSize(variant, key, variants[j].positions[key]);
                var inner = $('<div class="inner">'+variants[j].value+'</div>');
                inner.css('width',variant.css('width'));
                inner.css('height',variant.css('height'));
                if(variants[0].textStyle)
                    inner.css(variants[0].textStyle);
                if(variants[j].textStyle)
                    inner.css(variants[j].textStyle);
                variant.append(inner);
                if(element.attr("id").indexOf('manyOf')!=-1){//data.showIcons
                var icondiv = $('<div class="icon"></div>');
                    setSize(icondiv,"width",20);
                    setSize(icondiv,"height",20);
                    variant.append(icondiv);
                }
                element.append(variant);
                result.push(element);

                /*if(!variants[i].positions || !variants[i].positions.left){
                    if(colLeft+variant.outerWidth(true)<element.width()){
                        variant.css('left', colLeft+'px');
                        variant.css('top', rowTop+'px');
                        colLeft+=variant.outerWidth(true);
                        if(rowHeight<variant.outerHeight(true))
                            rowHeight=variant.outerHeight(true);
                    } else {
                        rowTop+=rowHeight;
                        colLeft=0;
                        rowHeight=variant.outerHeight(true);
                        variant.css('left', colLeft+'px');
                        variant.css('top', rowTop+'px');
                        colLeft+=variant.outerWidth(true);
                    }
               }*/
            }


            }
        
        }
        setTimeout(function(){test_select.updateVariantsPositions()},100);
        return $(result);
    },

    variantClick:function(e){
        if(!$(this).hasClass('clickable-selected')){
            if($(this).parent().data('blockType')=='oneOf'){
                $(this).siblings().removeClass('clickable-selected');//.removeClass('selected');
				$(this).siblings().removeClass('clickable-right');
                $(this).siblings().removeClass('clickable-wrong');
                $(this).siblings().removeClass('wrong_unchecked');
                if(!data[currentTask]) data[currentTask]={};
                data[currentTask][$(this).parent().data('blockNum')] = $(this).data('answer');
            } else {
                if(data[currentTask] && data[currentTask][$(this).parent().data('blockNum')])
                    data[currentTask][$(this).parent().data('blockNum')][$(this).data('answer')]=true;
                else {
                    if(!data[currentTask]) data[currentTask]={};
                    data[currentTask][$(this).parent().data('blockNum')]={};
                    data[currentTask][$(this).parent().data('blockNum')][$(this).data('answer')]=true;
                }
            }
            if($(this).hasClass('one'))
                $(this).addClass('clickable-selected');
            else{
				$(this).removeClass('clickable-right');
				$(this).removeClass('clickable-wrong');
				$(this).removeClass('wrong_unchecked');	
                if($(this).hasClass('selected')){
                    $(this).removeClass('selected');
					data[currentTask][$(this).parent().data('blockNum')][$(this).data('answer')]=false;
				
				}
                else
                    $(this).addClass('selected');
            }
        } else {
            $(this).removeClass('clickable-selected');
			$(this).removeClass('clickable-right');
            $(this).removeClass('clickable-wrong');
            $(this).removeClass('wrong_unchecked');
            if($(this).parent().data('blockType')=='oneOf')
                data[currentTask][$(this).parent().data('blockNum')] = false;
            else
                data[currentTask][$(this).parent().data('blockNum')][$(this).data('answer')]=false;
        }
        if(task[currentTask].check.autoCheck){
            lastElementId = $(this).parent().attr('id');
            check();

        }
    },

    check:function(){
        var checkResult = true;
        var tmpResult = false;
        var currentScore = 0;
        if(task[currentTask]['test_select'])
            for(var i=0;i<task[currentTask]['test_select'].length;i++){
                if(task[currentTask]['test_select'][i].type=='oneOf' || task[currentTask]['test_select'][i].type=='manyOf'){
                    var variants = task[currentTask]['test_select'][i].variants;
                    tmpResult = task[currentTask]['test_select'][i].type=='oneOf' ? false : true;
                    for(var j=1;j<variants.length;j++){
                        if(variants[j].right){
                            if(task[currentTask]['test_select'][i].type=='oneOf' && data[currentTask] && data[currentTask][i]==j){
                                tmpResult = true;
                                if(showCheckAnimation()) $('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').addClass('clickable-right');
                                currentScore+=parseInt($('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').data('weight'));
                            }
                            else {
                                if(task[currentTask]['test_select'][i].type=='manyOf')
                                    if(data[currentTask] && data[currentTask][i] && data[currentTask][i][j]){
                                        if(showCheckAnimation()) $('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').addClass('clickable-right');
                                        currentScore+=parseInt($('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').data('weight'));
                                    } else {
                                        tmpResult = false;
                                    }
                                
                            }
                        } else{
                            if(data[currentTask] && data[currentTask][i]==j){
                                if(showCheckAnimation()) $('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').addClass('clickable-wrong');
                                currentScore+=parseInt($('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').data('weight'));
                            }
                            if(data[currentTask] && data[currentTask][i] && data[currentTask][i][j]){
                                if(showCheckAnimation()) $('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').addClass('clickable-wrong');
                                currentScore+=parseInt($('#'+task[currentTask]['test_select'][i].type+i).children('.variant:eq('+(j-1)+')').data('weight'));
                                tmpResult = false;
                            }
                        }
                        
                    }
                    if(!tmpResult)
                         checkResult=false;
                }// else {tmpResult=true}

            }
            return {"result":checkResult,"score":currentScore};
    },
    "updateVariantsPositions":function(){
        $('.absoluteBlock').each(function(){
            var rowTop=0;
            var colLeft=0;
            var rowHeight=0;
            $(this).children().each(function(){
                if(colLeft+$(this).outerWidth(true)<$(this).parent().width()){
                    $(this).css('left', colLeft+'px');
                    $(this).css('top', rowTop+'px');
                    colLeft+=$(this).outerWidth(true);
                    if(rowHeight<$(this).outerHeight(true))
                        rowHeight=$(this).outerHeight(true);
                } else {
                    rowTop+=rowHeight;
                    colLeft=0;
                    rowHeight=$(this).outerHeight(true);
                    $(this).css('left', colLeft+'px');
                    $(this).css('top', rowTop+'px');
                    colLeft+=$(this).outerWidth(true);
                }
            })
        });
    },
    "stageSuccess":function(){
        for(var i=0;i<task[currentTask]['test_select'].length;i++){
            if(task[currentTask]['test_select'][i].stageSuccessAnimation){
                if(task[currentTask]['test_select'][i].stageSuccessAnimation.type == 'rightToPosition'){
                    $('#'+task[currentTask]['test_select'][i].type+i+' .clickable-right').animate({"top":getSize(task[currentTask]['test_select'][i].stageSuccessAnimation.top)+"px", "left":getSize(task[currentTask]['test_select'][i].stageSuccessAnimation.left)+"px"});
                }
            }
        }
    }
}